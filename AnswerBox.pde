class AnswerBox extends Box {  
  PVector leftButton;
  PVector rightButton;
  PVector answerIndicator;

  public AnswerBox(float x, float y, float w, float h, PVector lbOffset, PVector rbOffset) {
    pos = new PVector(x, y);
    size = new PVector(w, h);

    leftButton = new PVector(-size.x * 0.25 + lbOffset.x, size.y * 0.5 + lbOffset.y);
    rightButton = new PVector(size.x * 0.25 + rbOffset.x, size.y * 0.5 + rbOffset.y);
    
    
    answerIndicator = new PVector(lerp(leftButton.x, rightButton.x, 0.54), size.y * 0.3);
  }

  public void updateAlpha(int state) {
    if (state == QUESTION_SHOWING) {
      if (alpha < 255) {
        alpha += 20;
      } else { 
        if (alpha > 0) {
          alpha -= 20;
        }
      }
    }
  }

  public void update(int state) {
    updateAlpha(state);
  }

  public void display(PGraphics offscreen, int state, int type, String currentAnswer, int timeRemaining) {
    offscreen.push();
    offscreen.imageMode(CENTER);
    offscreen.textAlign(CENTER, CENTER);
    offscreen.textFont(ralewayFont);

    offscreen.translate(pos.x, pos.y);

    if (debug || (state == QUESTION_SHOWING || state == QUESTION_DISAPPEARING)) {
      if (!currentAnswer.equals("")) {
        //offscreen.fill(360, 0, 100);
        //offscreen.ellipse(answerIndicator.x, answerIndicator.y, BUTTON_DIAMETER, BUTTON_DIAMETER);
        offscreen.noFill();
        offscreen.strokeWeight(3);
        offscreen.stroke(0, 0, 100);
        offscreen.arc(answerIndicator.x, answerIndicator.y, BUTTON_DIAMETER, BUTTON_DIAMETER, -PI * 0.5, map(timeRemaining - millis(), 0, ANSWER_TIMEOUT, -PI * 0.5, PI * 1.5), OPEN);
        offscreen.fill(360, 0, 100, alpha);        
        offscreen.text(currentAnswer, answerIndicator.x, answerIndicator.y - RALEWAY_FONT_SIZE / 4);
      }

      if (type == NUMERIC) {
        offscreen.fill(360, 0, 100, alpha);
        offscreen.text("-", leftButton.x + RALEWAY_FONT_SIZE / 6, leftButton.y - RALEWAY_FONT_SIZE / 4);
        offscreen.text("+", rightButton.x + RALEWAY_FONT_SIZE / 6, rightButton.y - RALEWAY_FONT_SIZE / 6);
      } else {
        offscreen.fill(360, 0, 100, alpha);
        offscreen.text("No", leftButton.x + RALEWAY_FONT_SIZE / 5, leftButton.y - RALEWAY_FONT_SIZE / 5);
        offscreen.text("Sí", rightButton.x + RALEWAY_FONT_SIZE / 5, rightButton.y - RALEWAY_FONT_SIZE / 5);
      }

      rotatingButton.render(offscreen, leftButton.x, leftButton.y);
      rotatingButton.render(offscreen, rightButton.x, rightButton.y);
    }


    offscreen.pop();
  }

  public PVector getAbsoluteButtonPosition(int sub) {    
    PVector v = null;

    if (sub == 1) {
      v = PVector.add(pos, leftButton);
    } else if (sub == 2) {
      v = PVector.add(pos, rightButton);
    }

    return v;
  }

  public void handleTouch(int sub) {
  }
}
