class AnimatedObject extends Box {
  private PImage active;
  private AnimationData[] animations;
  private Animation currentAnimation;
  private int currentAnimationIndex;

  private PVector renderSize;
  private PVector sensorPosition;

  private PVector offset;

  private int alpha;
  private boolean alphaDirection;

  public AnimatedObject(String path, AnimationData[] anim, float x, float y, float w, float h, PVector o) {
    // BG
    active = loadImage(path + "active.png");

    animations = anim;

    pos = new PVector(x, y);
    size = new PVector(w, h);

    currentAnimationIndex = 0;

    sensorPosition = new PVector(size.x * 0.5, size.y * 0.5);
    PVector imageSize = new PVector(active.width, active.height);

    float percent = imageSize.x / w;
    renderSize = new PVector (w, imageSize.y / percent);

    offset = o;
  }

  public void updateAlpha(int state) {
    // Standby alpha
    if (state == STANDBY) {
      if (alpha > 0) {
        alpha -= 5;
      }
    } else {
      if (alpha < 255 ) {
        alpha += 80;
      }
    }


    if (alpha < 0) {
      alpha = 0;
    }

    if (alpha > 255) {
      alpha = 255;
    }
  }

  @Override
    public void update(int state) {
    super.update(state); 

    switch (state) {

    case STANDBY:

      if (currentAnimation != null) {
        currentAnimation = null;
        currentAnimationIndex++;

        if (currentAnimationIndex > animations.length - 1) {
          currentAnimationIndex = 0;
        }
      }
      break;

    default: 

      // Animations
      if (animations.length > 0) {
        if (currentAnimation != null) {
          currentAnimation.update();

          if (currentAnimation.shouldRemove()) {
            currentAnimation = null;

            System.gc();
            System.runFinalization();

            currentAnimationIndex++;

            if (currentAnimationIndex > animations.length - 1) {
              currentAnimationIndex = 0;
            }
          }
        }
      }
    }
  }

  public void display(PGraphics offscreen, int state) {
    offscreen.pushMatrix();
    offscreen.translate(pos.x, pos.y);

    if (debug) {
      offscreen.tint(360, 255);
    } else { 
      offscreen.tint(360, alpha);
    }
    offscreen.image(active, offset.x, offset.y, renderSize.x, renderSize.y);

    if (state != STANDBY) {
      if (currentAnimation != null) {
        offscreen.push();
        currentAnimation.render(offscreen, offset.x, offset.y, renderSize.x, renderSize.y);
        offscreen.pop();
      }
    }

    offscreen.popMatrix();
  }

  public boolean handleTouch() {
    if (currentAnimation == null) {
      currentAnimation = new Animation(animationLoader.loadAnimation(animations[currentAnimationIndex].getAnimationPath()));
      currentAnimation.setLoop(animations[currentAnimationIndex].getloopCount());
      if (currentAnimation.getLoopTimes() > 0) {
        currentAnimation.setAlpha(0);
      }
      if (animations[currentAnimationIndex].getInfinite()) {
        currentAnimation.setInfiniteLoop();
      }
      currentAnimation.play();
      touchSound.trigger();
      return true;
    } else {
      if (currentAnimation.getAlpha() >= 255) {
        currentAnimation.setDisappear(true);
        touchSound.trigger();
        return true;
      }
    }
    return false;
  }

  public Animation getCurrentAnimation() {
    return currentAnimation;
  }

  public void setAlpha(int alpha) {
    this.alpha = alpha;
  }

  public int getAlpha() {
    return alpha;
  }
}
