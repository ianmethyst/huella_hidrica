class Section {
  private int id;
  private int state;

  private AnimatedObject[] ao;
  private QuestionBox qb;
  private AnswerBox ab;

  private Step[] steps;
  private int currentStep;
  private int lastStateChange;

  private PVector pos;
  private PVector size;

  float colour;

  int timeRemaining;
  int nextAction;

  boolean flashed;
  int nextFlashTime;
  int lastInteractionTime;

  String extraText;

  public Section(String json, int id, float x, float y) {
    JSONObject data = null;
    this.id = id;
    state = STANDBY;

    try {
      data = loadJSONObject("data/" + json + ".json");
    } 
    catch (Exception e) {
      e.printStackTrace();
      println("ERROR: couldn't load json file (" + json + ".json)");
      return;
    }

    pos = new PVector(x, y);
    size = new PVector(data.getFloat("width") * PIXELS_PER_CM, height);

    // Steps
    JSONArray questions = data.getJSONArray("questions");
    steps = new Step[questions.size()];
    currentStep = 0;

    for (int i = 0; i < questions.size(); i++) {
      JSONObject object = questions.getJSONObject(i);

      // Question
      String question = object.getString("question");

      // Type
      int type;
      String t = object.getString("type");
      switch (t) {
      case "numeric":
        type = NUMERIC;
        break;

      case "boolean":
        type = BOOLEAN;
        break;

      default:
        type = -1;
        println("ERROR, step type not set");
        break;
      }

      // Results 
      String[] results;

      JSONArray _results = object.getJSONArray("results");

      results = new String[_results.size()];

      for (int r = 0; r < _results.size(); r++) { 
        results[r] = _results.getString(r);
      }

      // Answers
      Answer[] answers;
      JSONArray _answers = object.getJSONArray("answers");
      answers = new Answer[_answers.size()];

      for (int a = 0; a < _answers.size(); a++) {
        JSONObject _answer = _answers.getJSONObject(a);

        String _action = _answer.getString("action");
        int action = -1;

        switch (_action) {
        case "STOP":
          action = STOP;
          break;
        case "SHOW_RESULT":
          action = SHOW_RESULT;
          break;
        case "SKIP":
          action = SKIP;
          break;
        case "SHOW_RESULT_STOP": 
          action = SHOW_RESULT_STOP;
          break;
        case "TAP": 
          action = TAP;
          break;
        }

        if (action == -1) {
          println("ERROR: action not defined");
        }

        answers[a] = new Answer(_answer.getString("text"), _answer.getInt("linkedResult"), action);
      }

      // Extra
      steps[i] = new Step(question, type, results, answers);
      lastStateChange = millis();
    }

    extraText = data.getString("extraInfo");

    // Animations
    JSONArray _animatedObjects = data.getJSONArray("animatedObjects");
    ao = new AnimatedObject[_animatedObjects.size()];

    for (int a = 0; a < ao.length; a++) {
      JSONObject _ao = _animatedObjects.getJSONObject(a);

      AnimationData[] animations;
      JSONArray _animations = _ao.getJSONArray("animations");
      animations = new AnimationData[_animations.size()];

      for (int i = 0; i < animations.length; i++) {
        JSONObject currentAnimation = _animations.getJSONObject(i);

        String path =  sketchPath() + "/" + _ao.getString("img") +  currentAnimation.getInt("id");
        int loop = currentAnimation.getInt("loopTimes");
        boolean inf = currentAnimation.getBoolean("infinite");

        animations[i] = new AnimationData(path, loop, inf);
      }

      JSONObject _offset = _ao.getJSONObject("offset");
      PVector offset = new PVector(_offset.getFloat("x") * PIXELS_PER_CM, _offset.getFloat("y") * PIXELS_PER_CM);

      ao[a] = new AnimatedObject(_ao.getString("img"), animations, 0, size.y * 0.35, size.x, 40 * PIXELS_PER_CM, offset);
    }

    // Button offsets
    PVector lb;
    PVector rb;

    JSONObject _offsets = data.getJSONObject("buttonOffsets");
    JSONObject _lb = _offsets.getJSONObject("left");
    JSONObject _rb = _offsets.getJSONObject("right");

    lb = new PVector(_lb.getFloat("x"), _lb.getFloat("y")).mult(PIXELS_PER_CM);
    rb = new PVector(_rb.getFloat("x"), _rb.getFloat("y")).mult(PIXELS_PER_CM);

    qb = new QuestionBox(size.x * 0.5, size.y * 0.15, size.x * 0.8, size.y * 0.27);
    ab = new AnswerBox(size.x * 0.5, size.y * 0.75, size.x * 0.9, size.y * 0.27, lb, rb);

    colour = random(360);
  }

  public void update() {
    if (millis() > lastStateChange + 30000) {
      lastStateChange = millis();
      println("resetting from " + state + " to STANDBY (0)");

      resetSection();
    }


    switch (state) {
    case STANDBY:
      if (!flashed && millis() > nextFlashTime) {
        for (AnimatedObject object : ao) {
          object.setAlpha(255);
        }
        flashed = true;
      }
      break;

    case QUESTION_SHOWING:
      // If the timeout is reached and an answer was selected, qb begins to disappear
      if (millis() > timeRemaining) {
        if (steps[currentStep].getCurrentAnswerText().equals("")) {
          resetSection();
        } else {
          state = QUESTION_DISAPPEARING;
          lastStateChange = millis();
        }
      }

      break;

    case QUESTION_DISAPPEARING:
      // Ather qb finishes disappearing, the answer action trigger determines
      // what happen next, and qb begins to appear again
      if (qb.getAlpha() <= 0) {
        switch (steps[currentStep].getCurrentAnswer().getAction()) {
        case STOP:
          resetSection();
          break;

        case SHOW_RESULT:
        case SHOW_RESULT_STOP:
          state = RESULT_SHOWING;
          lastStateChange = millis();
          resetTime(RESULT_TIMEOUT);
          break;

        case SKIP:
          currentStep++;
          if (currentStep == steps.length) {
            state = EXTRA_SHOWING;
            lastStateChange = millis();
          } else {
            state = QUESTION_SHOWING;
            lastStateChange = millis();
            resetTime(ANSWER_TIMEOUT);
          }
          break;
        }
      }

      break;

    case RESULT_SHOWING:
      // If qb is appearing or shown, and the timeout for the result is
      // reached, the result is set as shown, and qb begins to disappear again
      if (millis() > timeRemaining) {
        state = RESULT_DISAPPEARING;
        lastStateChange = millis();
      }
      break;

    case RESULT_DISAPPEARING:
      // If the result was shown and qb finished disappearing, the action determines what step or
      // state should be entered
      if (qb.getAlpha() == 0) {
        switch (steps[currentStep].getCurrentAnswer().getAction()) {
        case SHOW_RESULT: 
          currentStep++;
          if (currentStep == steps.length) {
            state = EXTRA_SHOWING;
            resetTime(RESULT_TIMEOUT);
            lastStateChange = millis();
          } else {
            resetTime(ANSWER_TIMEOUT);
            state = QUESTION_SHOWING;
            lastStateChange = millis();
          }
          break;

        case SHOW_RESULT_STOP: 
          resetSection();
          break;

        default:
          resetSection();
        }
      }

      break;

    case EXTRA_SHOWING:
      if (millis() > timeRemaining) {
        state = EXTRA_DISAPPEARING;
        lastStateChange = millis();
      }
      break;

    case EXTRA_DISAPPEARING:
      if (qb.getAlpha() == 0) {
        resetSection();
        lastStateChange = millis();
      }
      break;

    default:
      resetSection();
    }


    for (AnimatedObject animatedObject : ao) {
      animatedObject.update(state);
    }

    qb.update(state);
    ab.update(state);
  }

  public void display(PGraphics offscreen) {
    offscreen.pushMatrix();
    offscreen.translate(pos.x, pos.y);

    if (debug) {
      offscreen.pushStyle();
      offscreen.fill(colour, 50, 50);
      offscreen.rectMode(CORNER);
      offscreen.rect(0, 0, size.x, size.y);
      offscreen.stroke(180, 100, 100);
      offscreen.line(size.x / 2, 0, size.x / 2, size.y);
      offscreen.fill(0, 0, 100);
      offscreen.text("state: " + state, size.x / 2, BARLOW_FONT_SIZE / 2);

      offscreen.popStyle();
    }

    for (AnimatedObject animatedObject : ao) {
      animatedObject.display(offscreen, state);
    }

    if (currentStep < steps.length) {
      ab.display(offscreen, state, steps[currentStep].getType(), steps[currentStep].getCurrentAnswerText(), timeRemaining);
    }

    switch (state) {
    case QUESTION_SHOWING:
    case QUESTION_DISAPPEARING:
      qb.display(offscreen, steps[currentStep].getQuestion());
      break;

    case RESULT_SHOWING:
    case RESULT_DISAPPEARING:
      qb.display(offscreen, steps[currentStep].getCurrentAnswerResult());
      break;

    case EXTRA_SHOWING:
    case EXTRA_DISAPPEARING:
      qb.display(offscreen, extraText);
      break;
    }

    offscreen.popMatrix();
  }

  public void handleTouch(int i, int sub) {
    if (millis() > lastInteractionTime + 500) {
      if (id == i) {
        println(i, sub);
        switch (state) {
        case STANDBY:
          if (sub == 0 || sub == 3) {
            state = QUESTION_SHOWING;
            resetTime(ANSWER_TIMEOUT);
            lastInteractionTime = millis();
            lastStateChange = millis();
            touchSound.trigger();
          }
          break;

        case QUESTION_SHOWING:
          if (steps[currentStep].getType() == NUMERIC) {
            if (sub == 1) {
              steps[currentStep].cycleAnswersLeft();
              resetTime(ANSWER_TIMEOUT);
              lastInteractionTime = millis();
              touchSound.trigger();
            } else if (sub == 2) {
              steps[currentStep].cycleAnswersRight();
              resetTime(ANSWER_TIMEOUT);
              lastInteractionTime = millis();
              touchSound.trigger();
            }
          } else if (steps[currentStep].getType() == BOOLEAN) {
            if (sub == 1) {
              steps[currentStep].setCurrentAnswer(0);
              resetTime(ANSWER_TIMEOUT);
              lastInteractionTime = millis();
              touchSound.trigger();
            } else if (sub == 2) {
              steps[currentStep].setCurrentAnswer(1);
              resetTime(ANSWER_TIMEOUT);
              lastInteractionTime = millis();
              touchSound.trigger();
            }
          }

        case QUESTION_DISAPPEARING:
        case RESULT_SHOWING:
        case RESULT_DISAPPEARING:
        case EXTRA_SHOWING:
        case EXTRA_DISAPPEARING:
          if (sub == 0) {
            if (ao[0].handleTouch()) {
              lastInteractionTime = millis();
            }

            if (state == QUESTION_SHOWING) {
              resetTime(ANSWER_TIMEOUT);
            }
          }

          if (sub == 3) {
            if (ao.length > 1 && ao[1] != null) {
              if (ao[1].handleTouch()) {
                lastInteractionTime = millis();
              };

              if (state == QUESTION_SHOWING) {
                resetTime(ANSWER_TIMEOUT);
              }
            }
          }
          break;
        }
      }
    }
  }

  private void resetSection() {
    state = STANDBY;
    lastStateChange = millis();
    currentStep = 0;
    for (Step s : steps) {
      s.resetCurrentAnswer();
    }
  }

  private void resetTime(int timeout) {
    timeRemaining = millis() + timeout;
  }

  public PVector getPos() {
    return pos;
  }

  public PVector getSize() {
    return size;
  }

  public AnswerBox getAnswerBox() {
    return ab;
  }

  public AnimatedObject getAnimatedObject(int index) {
    return ao[index];
  }

  public int getId() {
    return id;
  }

  public int getState() {
    return state;
  }

  public void setNextFlashTime(int time) {
    nextFlashTime = time;
    flashed = false;
  }

  public boolean didFlash() {
    return flashed;
  }
}
