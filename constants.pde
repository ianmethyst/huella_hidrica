// States
final int STANDBY = 0;
final int QUESTION_SHOWING = 1;
final int QUESTION_DISAPPEARING = 2;
final int RESULT_SHOWING = 3;
final int RESULT_DISAPPEARING = 4;
final int EXTRA_SHOWING = 5;
final int EXTRA_DISAPPEARING = 6;

//switch (state) {
//case STANDBY:

//  break;

//case QUESTION_SHOWING:

//  break;

//case QUESTION_DISAPPEARING:

//  break;

//case RESULT_SHOWING:

//  break;

//case RESULT_DISAPPEARING:

//  break;

//case EXTRA_SHOWING:

//  break;

//case EXTRA_DISAPPEARING:
//  break;
//}


// Answers types
final int NUMERIC = 0;
final int BOOLEAN = 1;

// Answer actions
final int SHOW_RESULT = 0;
final int SKIP = 1;
final int STOP = 2;
final int SHOW_RESULT_STOP = 3;
final int TAP = 4;

// Time for answers
final int ANSWER_TIMEOUT = 4000;
final int RESULT_TIMEOUT = 8000;

// Colors
final color COLOR1 = #bdffdc;
final color COLOR2 = #00aaff;
final color COLOR3 = #00009c;

// Text variables
final int BARLOW_FONT_SIZE = 23;
final int RALEWAY_FONT_SIZE = 30;
final int FONT_LEADING = 30;

// Time
final int ANSWER_LIFESPAN = 4000;

// Size
//final float PIXELS_PER_CM = 3.413; // 1024p 
//final float PIXELS_PER_CM = 6.4; // 1920p
final float PIXELS_PER_CM = 6.826; // 2048p

final float BUTTON_DIAMETER = 65;

// Serial
final int MAX_ARDUINOS = 3;
final int LF = 10;
final int BAUD_RATE = 4800;

// Drop count file
final String DROP_COUNT_FILE = "dropcount.txt";
final int DROP_FRAME = 37;
