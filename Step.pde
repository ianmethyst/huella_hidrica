class Step {
  private String question;
  private int type;
  private String[] results;
  private Answer[] answers;

  int currentAnswer;
  boolean resultShown;

  public Step(String q, int t, String[] r, Answer[] a) {
    question = q;
    type = t;
    results = r;
    answers = a;

    currentAnswer = -1;
  }

  public String getQuestion() {
    return question;
  }

  public String[] getResults() {
    return results;
  }

  public Answer[] getAnswers() {
    return answers;
  }

  public String getResult(int index) {
    return results[index];
  }

  public Answer getAnswer(int index) {
    return answers[index];
  }

  public String getAnswerResult(int index) {
    return results[answers[index].getResultIndex()];
  }

  public String getCurrentAnswerResult() {
    return results[answers[currentAnswer].getResultIndex()];
  }

  public int getType() {
    return type;
  }

  public Answer getCurrentAnswer() {
    if (currentAnswer == -1) {
      return null;
    } else {
      return answers[currentAnswer];
    }
  }

  public void resetCurrentAnswer() {
    currentAnswer = -1;
  }

  public String getCurrentAnswerText() {
    if (getCurrentAnswer() != null) {
      return answers[currentAnswer].getText();
    } else {
      return "";
    }
  }
  
  public void setCurrentAnswer(int c) {
    currentAnswer = c;
  }

  public void cycleAnswersRight() {
    currentAnswer++;

    if (currentAnswer > answers.length - 1) {
      currentAnswer = 0;
    }
  }

  public void cycleAnswersLeft() {
    currentAnswer--;

    if (currentAnswer < 0) {
      currentAnswer = answers.length - 1;
    }
  }
}
