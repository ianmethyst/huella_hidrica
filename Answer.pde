class Answer {
  private String text;
  private int resultIndex;
  private int action;
  
  public Answer(String t, int i, int a) {
    text = t;
    resultIndex = i;
    action = a;
  }

  public String getText() {
    return text;
  }

  public int getResultIndex() {
    println(resultIndex);
    return resultIndex;
  }
  
  public int getAction() {
    return action;
  }
}
