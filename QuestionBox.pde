class QuestionBox extends Box {
  private PVector pos;
  private PVector size;

  public QuestionBox(float x, float y, float w, float h) {
    pos = new PVector(x, y);
    size = new PVector(w, h);
  }

  public void updateAlpha(int state) {
    switch (state) {

    case STANDBY: 
      if (alpha > 0) {
        alpha -= 5;
      }

      break;

    case EXTRA_SHOWING:
    case RESULT_SHOWING:
    case QUESTION_SHOWING: 
      if (alpha < 255) {
        alpha += 20;
      }

      break;

    case EXTRA_DISAPPEARING:
    case RESULT_DISAPPEARING:
    case QUESTION_DISAPPEARING:
      if (alpha > 0) {
        alpha -= 20;
      }
    }
  }

  public void update(int state) {
    updateAlpha(state);
  }

  public void display(PGraphics offscreen, String t) {
    offscreen.push(); 
    //offscreen.translate(pos.x, pos.y);
    offscreen.fill(360, 0, 100, alpha);
    offscreen.text(t, pos.x -size.x / 2, pos.y - size.y / 2, pos.x + size.x / 2, pos.y + size.y / 2);
    offscreen.pop();
  }
}
