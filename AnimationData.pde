class AnimationData {
  private String animationPath;
  private int loopCount;
  private boolean infinite;

  public AnimationData(String p, int l, boolean i) {
    animationPath = p;
    loopCount = l;
    infinite = i;
  }

  public String getAnimationPath() {
    return animationPath;
  }

  public int getloopCount() {
    return loopCount;
  }

  public boolean getInfinite() {
    return infinite;
  }
}
