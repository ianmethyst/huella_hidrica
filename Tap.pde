class Tap {
  AnimatedObject ao;

  PVector pos;
  PVector size;

  boolean leaking;
  int nextLeakingTime;
  int drops;

  public Tap(String json, int id, float x, float y) {
    JSONObject data = null;

    try {
      data = loadJSONObject("data/" + json + ".json");
    } 
    catch (Exception e) {
      e.printStackTrace();
      println("ERROR: couldn't load json file (" + json + ".json)");
      return;
    }

    pos = new PVector(x, y);
    size = new PVector(data.getFloat("width") * PIXELS_PER_CM, height);

    // Drops

    int count = 0;
    // Get drop count from file if exists
    try {
      count = Integer.parseInt(loadStrings(DROP_COUNT_FILE)[0]);
    } 
    catch (Exception e) {
      e.printStackTrace();
      println(DROP_COUNT_FILE + " not found");
      count = 0;
    }

    drops = count;

    // Animated Object
    JSONArray _animatedObjects = data.getJSONArray("animatedObjects");
    JSONObject _ao = _animatedObjects.getJSONObject(0);

    AnimationData animation = null;

    JSONArray _animations = _ao.getJSONArray("animations");

    for (int i = 0; i < _animations.size(); i++) {
      JSONObject currentAnimation = _animations.getJSONObject(i);

      String path =  sketchPath() + "/" + _ao.getString("img") +  currentAnimation.getInt("id");
      int loop = currentAnimation.getInt("loopTimes");
      boolean inf = currentAnimation.getBoolean("infinite");

      animation = new AnimationData(path, loop, inf);
    }

    JSONObject _offset = _ao.getJSONObject("offset");
    PVector offset = new PVector(_offset.getFloat("x") * PIXELS_PER_CM, _offset.getFloat("y") * PIXELS_PER_CM);

    AnimationData[] animations = {animation};

    ao = new AnimatedObject(_ao.getString("img"), animations, 0, size.y * 0.35, size.x, 40 * PIXELS_PER_CM, offset);
    ao.handleTouch();
  }

  void update() {
    ao.update(-1);

    if (leaking) {
      if (ao.getCurrentAnimation().getCurrentFrameIndex() == DROP_FRAME) {
        drops++;
        saveValue();
      }
    } else {
      if (millis() > nextLeakingTime) {
        leaking = true;
        ao.getCurrentAnimation().play();
      }
    }
  }

  void display(PGraphics offscreen) {
    offscreen.pushMatrix();

    String formatted = null;


    if (millis() % 30000 <= 9000 || millis() % 30000 == 0) {
      formatted = drops + " gotas\nderramadas";
    } else if (millis() % 30000 > 9000 && millis() % 30000 <= 19000) {
      formatted = drops + " ml\nderramados";
    } else if (millis() % 30000 > 19000) {
      formatted = drops / 1000 + " litros\nderramados";
    } else {
      formatted = drops + " gotas\nderramadas";
    }

    offscreen.pushStyle();
    offscreen.textAlign(RIGHT, BOTTOM);
    offscreen.textFont(barlowFont);
    offscreen.fill(0, 0, 100);
    offscreen.text(formatted, offscreen.width - BARLOW_FONT_SIZE / 2, offscreen.height  - BARLOW_FONT_SIZE / 2);
    if (!leaking) {
      offscreen.textAlign(RIGHT, TOP);
      offscreen.text("¡Bien, detuviste la pérdida de agua!", offscreen.width - BARLOW_FONT_SIZE / 2, 0);
    }
    offscreen.translate(pos.x, pos.y);
    offscreen.popStyle();


    if (debug) {
      offscreen.pushStyle();
      offscreen.fill(10, 50, 50);
      offscreen.rectMode(CORNER);
      offscreen.rect(0, 0, size.x, size.y);
      offscreen.stroke(180, 100, 100);
      offscreen.line(size.x / 2, 0, size.x / 2, size.y);
      offscreen.popStyle();

      offscreen.text(drops, size.x / 2, size.y / 2);
    }

    ao.display(offscreen, -1);
    offscreen.popMatrix();
  }


  void handleTouch() {
    if (leaking) {
      touchSound.trigger();
      ao.getCurrentAnimation().pause();
      ao.getCurrentAnimation().jumpTo(46);
      leaking = false;
      nextLeakingTime = millis() + 45000;
    }
  }

  void saveValue() {
    String[] d = {Integer.toString(drops)};
    saveStrings(DROP_COUNT_FILE, d);
  }
}
