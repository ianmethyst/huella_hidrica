import java.io.File;
import java.io.FilenameFilter;

import ianmethyst.animation.Animation;
import ianmethyst.animation.AnimationLoader;
import ianmethyst.keystone.*;

import processing.serial.*; 
import ddf.minim.*;

Minim minim;
AudioSample touchSound;

AnimationLoader animationLoader;
Keystone ks;

Serial[] arduinos;

boolean debug = false;

String[] sectionNames = { 
  "auto", 
  "lavarropas", 
  "cocina", 
  "lavatorio", 
  "inodoro", 
  "ducha", 
  "plantas", 
};

Section[] sections;
Tap tap;

int[] nextFlashTime;

PGraphics offscreen;
CornerPinSurface surface;

PFont barlowFont, ralewayFont;

Animation rotatingButton;

void setup() {
  //size(2048, 768, P3D);
  fullScreen(P3D, SPAN);
  frameRate(24);

  barlowFont = createFont("data/font/Barlow-Regular.otf", BARLOW_FONT_SIZE);
  ralewayFont = createFont("data/font/Raleway-Bold.ttf", RALEWAY_FONT_SIZE);

  minim = new Minim(this);
  touchSound = minim.loadSample("sound/touchSound.mp3", 2048);

  animationLoader = new AnimationLoader(this);
  ks = new Keystone(this);

  rotatingButton = new Animation(animationLoader.loadAnimation(sketchPath() + "/data/img/botones/rotatingButton/"));
  rotatingButton.setInfiniteLoop();
  rotatingButton.play();

  surface = ks.createCornerPinSurface(width, height, 20);
  //surface = ks.createCornerPinSurface(2048, 768, 20);
  surface.setSelected(true);

  offscreen = createGraphics(width, height, P2D);
  //offscreen = createGraphics(2048, 768, P2D);

  offscreen.beginDraw();
  offscreen.textAlign(CENTER, CENTER);
  offscreen.textFont(barlowFont);
  offscreen.textLeading(FONT_LEADING);
  offscreen.imageMode(CORNER);
  offscreen.noStroke();
  offscreen.colorMode(HSB, 360, 100, 100);
  offscreen.endDraw();

  sections = new Section[sectionNames.length];

  for (int i = 0; i < sections.length; i++) {
    if (i < 1) {
      // First section
      sections[i] = new Section(sectionNames[i], i, (width / sections.length + 1) * i, 0);
    } else {
      // Subsequent sections
      sections[i] = new Section(sectionNames[i], i, sections[i - 1].getPos().x + sections[i - 1].getSize().x, 0);
    }
  }

  tap = new Tap("canilla", sections.length, sections[sections.length - 1].getPos().x + sections[sections.length - 1].getSize().x, 0);

  nextFlashTime = new int[sections.length];
  generateNextFlashTime(4000, 3000);


  arduinos = new Serial[3];

  for (int i = 0; i < Serial.list().length; i++) {
    String s = Serial.list()[i];
    if ( s.contains("USB") || s.contains("ACM")) {
      for (int j = 0; j < arduinos.length; j++) {
        if (arduinos[j] == null) {
          arduinos[j] = new Serial(this, s, BAUD_RATE);
          arduinos[j].bufferUntil(LF);
          break;
        }
      }
    }
  }

  try {
    ks.load();
  } 
  catch (Exception e) {
    e.printStackTrace();
  }
}

void draw() {
  if (ks.isCalibrating()) {
    background(255, 0, 255);
  } else {
    background(0);
  }

  int counter = 0;
  for (int i : nextFlashTime) {
    if (millis() > i) {
      counter++;
    }
  }

  if (counter == sections.length) {
    generateNextFlashTime(10000, 4000);
  }

  rotatingButton.update();

  offscreen.beginDraw();
  offscreen.clear();

  for (Section s : sections) {
    s.update();
    s.display(offscreen);
  }

  tap.update();
  tap.display(offscreen);

  offscreen.endDraw();

  surface.render(offscreen);

  if (ks.isCalibrating()) {
    pushStyle();
    fill(0, 255, 255);
    ellipse(mouseX, mouseY, 20, 20);
    popStyle();
  }

  if (debug) {
    pushStyle();
    stroke(255, 255, 0);
    line(1024, 0, 1024, height);
    popStyle();
  }
}

void keyPressed() {
  switch(key) {
  case 'c':
  case 'C':
    ks.toggleCalibration();
    break;

  case 'd':
  case 'D':
    debug = !debug;
    break;

  case 'l':
  case 'L':
    // loads the saved layout
    ks.load();
    break;

  case 's':
  case 'S':
    // saves the layout
    ks.save();
    break;

  case '0':
  case '1':
  case '2':
  case '3':
    PVector transformedMouse = surface.getTransformedCursor(mouseX, mouseY);
    int id = int(map(transformedMouse.x, 0, offscreen.width, 0, sectionNames.length));

    int parsedKey = Integer.parseInt(str(key));

    for (Section s : sections) {
      s.handleTouch(id, parsedKey);
    }

    break;

  case '6':
    tap.handleTouch();
    break;
  }
}

void serialEvent(Serial s) {
  String event = s.readString();
  println(event);

  String[] splitted = event.split("_");

  int id = Integer.parseInt(splitted[0].substring(0, 1));
  id--;
  int sub = Integer.parseInt(splitted[1].substring(0, 1));

  if (id == sections.length -1 ) {
    tap.handleTouch();
  } else {
    for (Section sec : sections) {
      sec.handleTouch(id, sub);
    }
  }
}

void generateNextFlashTime(int time, int randomness) {  
  for (int i = 0; i < sections.length; i++) {
    nextFlashTime[i] = int(random( millis() + time - randomness, millis() + time + randomness));

    sections[i].setNextFlashTime(nextFlashTime[i]);
  }
}
