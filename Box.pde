abstract class Box {
  protected PVector pos;
  protected PVector size;
  
  int alpha = 0;

  public PVector getPos() {
    return pos;
  }

  public PVector getSize() {
    return size;
  }
  
  public void update(int state) {
   updateAlpha(state); 
  }
  
  public int getAlpha() {
   return alpha; 
  }
  
  abstract void updateAlpha(int state);
}
